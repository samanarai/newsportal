<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class News extends Model
{
    protected $table = 'news';

    protected $fillable = ['category_id','author_id', 'title', 'slug', 'feature_image', 'status', 'meta_keyword', 'meta_description', 'short_description','viewscount', 'description', 'created_by', 'updated_by'];

    function category()
    {
        return $this->belongsTo(Category::class);
    }
    function author()
    {
        return $this->belongsTo(Author::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function comments()
    {
        return $this->HasMany(Comment::class);
    }

    public function newstypes()
    {
        return $this->belongsToMany(NewsType::class, 'news_newstype', 'news_id');
    }

}
