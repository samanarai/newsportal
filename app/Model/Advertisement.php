<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    protected $table = 'advertisements';

    protected $fillable = ['page_id','title','rank','description','image','status','expired_at'];

    public function page()
    {
        return $this->belongsTo(Page::class);
    }
}
