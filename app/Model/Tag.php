<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'tags';

    protected $fillable = ['name','rank','slug','meta_keyword','meta_description','status','created_by', 'updated_by'];

    public function news()
    {
        return $this->belongsToMany(News::class);
    }

}
