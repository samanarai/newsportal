<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';

    protected $fillable = ['page_name','url','title','description','short_description','slug','static_key','status','created_by','updated_by'];

    public function advertisements()
    {
        return $this->hasMany(Advertisement::class);
    }
}
