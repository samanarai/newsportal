<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $table = 'authors';

    protected $fillable = ['author_id','name','delegation','image','status'];

    function news()
    {
        return $this->hasMany(News::class);
    }
}
