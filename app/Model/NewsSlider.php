<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NewsSlider extends Model
{
    protected $table = 'news_sliders';

    protected $fillable = ['title','slug','comment','image','link','short_description','description','meta_keyword','meta_description', 'status','created_by', 'updated_by'];
}
