<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\SearchResult;


class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = ['name', 'rank', 'slug','status', 'meta_keyword', 'meta_description','description','meta_title','feature_image','created_by', 'updated_by'];

    function news()
    {
        return $this->hasMany(News::class);
    }
//
    public function Searchable()
    {
        $url = route('frontend.category', $this->id);

        return new SearchResult(
            $this,
            $this->name,
            $url
        );
    }
}
