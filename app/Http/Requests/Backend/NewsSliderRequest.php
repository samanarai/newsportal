<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class NewsSliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|string|max:191|unique:news_sliders'.(request()->method()=="POST" ? '' : ',title,'.$this->id),
            'slug'=>'required|string|max:191|unique:news_sliders'.(request()->method()=="POST" ? '' : ',slug,'.$this->id),
            'photo'=>'required|max:1024',
            'comment'=>'required|string',
            'link'=>'required|string',
            'meta_keyword'=>'required|string|max:191',
            'meta_description'=>'required|string|max:191',
            'short_description'=>'required|string|max:191',
            'description'=>'required|string',
        ];

    }
    function messages()
    {
        return[
//            'title.required'=>'Please Enter Title',
//            'slug.required'=>'Please Enter Slug',
//            'link.required'=>'Please Enter Link',
//            'comment.required'=>'Please Enter Comment',
            'photo.required'=>'Please Enter Image',
//            'meta_keyword.max'=>'Please Enter String',
//            'meta_description.max'=>'Plese Enter String',
//            'short_description.max'=>'Plese Enter String',
//            'description.required'=>'Plese Enter String',

        ];
    }
}
