<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class TagRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|string|max:191|unique:tags'.(request()->method()=="POST" ? '' : ',name,'.$this->id),
            'slug'=>'required|string|max:191|unique:tags'.(request()->method()=="POST" ? '' : ',slug,'.$this->id),
            'meta_description'=>'required|string',
            'meta_keyword'=>'required|string',
            'rank'=>'required|integer',
        ];

    }
    function messages()
    {
        return[
            'name.required'=>'Please Enter Name',
            'slug.required'=>'Please Enter Slug',
            'rank.required'=>'Please Enter Rank',
            'meta_description.required'=>'Please Enter Meta Description',
            'meta_keyword.required'=>'Please Enter Meta Keyword',
        ];
    }
}