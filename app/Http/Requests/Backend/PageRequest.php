<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class PageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page_name'=>'required|string',
            'url'=>'required|string',
            'title'=>'required|string|max:191|unique:pages'.(request()->method()=="POST" ? '' : ',title,'.$this->id),
            'slug'=>'required|string|max:191|unique:pages'.(request()->method()=="POST" ? '' : ',slug,'.$this->id),
            'description'=>'required|string',
            'short_description'=>'required|string',
        ];

    }
    function messages()
    {
        return[
            'page_name.required'=>'Please Enter Page  Name',
            'slug.required'=>'Please Enter Slug',
            'url.required'=>'Please Enter Url',
            'title.required'=>'Please Enter Title',
            'description.required'=>'Please Enter Description',
            'short_description.required'=>'Please Enter Short Description',
        ];
    }
}