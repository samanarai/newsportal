<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class ConfigurationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name'=>'required|string',
            'editor'=>'required|string',
            'sub_editor'=>'required|string',
            'email'=>'required|string',
            'phone'=>'required|integer',
            'website'=>'required|string',
            'address'=>'required|string',
            'fav_icon'=>'required|string|max:1024',
            'fb_link'=>'required|string',
            'twitter_link'=>'required|string',
            'insta_link'=>'required|string',
            'pinterest_link'=>'required|string',
            'vimeo_link'=>'required|string',
            'google_link'=>'required|string',

        ];

    }
    function messages()
    {
        return[
            'name.required'=>'Please Enter Name',
            'editor.required'=>'Please Enter Editor',
            'sub_editor.required'=>'Please Enter Sub Editor',
            'email.required'=>'Please Enter Email',
            'website.required'=>'Please Enter Website',
            'phone.required'=>'Please Enter Phone',
            'address.required'=>'Please Enter Address',
            'fav_icon.required'=>'Please Enter Fav Icon',
            'fb_link.required'=>'Please Enter Fb Link',
            'twitter_link.required'=>'Please Enter twitter link',
            'insta_link.required'=>'Please Enter insta link',
            'youtube_link.required'=>'Please Enter youtube link',
            'pinterest_link.required'=>'Please Enter pinterest link',
            'vimeo_link.required'=>'Please Enter vimeo link',
            'google_link.required'=>'Please Enter google link',
        ];
    }
}
