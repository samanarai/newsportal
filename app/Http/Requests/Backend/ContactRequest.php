<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|string',
            'email'=>'required|string',
            'website'=>'required|string',
            'message'=>'required|string',
        ];
    }
    function messages()
    {
        return[
            'name.required'=>'Please Enter Name',
            'email.required'=>'Please Enter Email',
            'website.required'=>'Please Enter Website',
            'message.max'=>'Please Enter Message',

        ];
    }
}
