<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class NewsTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|string|max:191|unique:newstypes'.(request()->method()=="POST" ? '' : ',name,'.$this->id),
            'slug'=>'required|string|max:191|unique:newstypes'.(request()->method()=="POST" ? '' : ',slug,'.$this->id),
            'photo'=>'required|max:1024',
        ];

    }
    function messages()
    {
        return[
//            'name.required'=>'Please Enter Name',
//            'slug.required'=>'Please Enter Slug',
            'photo.required'=>'Please Enter Image',
        ];
    }
}
