<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class AdvertisementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'page_id'=>'required|integer',
            'title'=>'required|string',
            'rank'=>'required|string',
            'expired_at'=>'required',
            'description'=>'required|string',
            'photo'=>'required|max:1024',
        ];

    }
    function messages()
    {
        return[
            'photo.required'=>'Please Enter Image',
        ];
    }
}
