<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'category_id'=>'required|integer',
            'title'=>'required|string|max:191|unique:news'.(request()->method()=="POST" ? '' : ',title,'.$this->id),
            'slug'=>'required|string|max:191|unique:news'.(request()->method()=="POST" ? '' : ',slug,'.$this->id),
            'photo'=>'required|max:1024',
            'meta_keyword'=>'required|string|max:191',
            'meta_description'=>'required|string|max:191',
            'short_description'=>'required|string|max:191',
            'description'=>'required|string',
        ];

    }
    function messages()
    {
        return[
            'photo.required'=>'Please Enter Image',

        ];
    }
}
