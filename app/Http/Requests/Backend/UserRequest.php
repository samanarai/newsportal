<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|string',
            'email'=>'required|unique:users,email|string',
            'password'=>'required|string',
            'address'=>'required|string',
            'phone'=>'required|string',
            'description'=>'required|string',
            'photo'=>'required|max:1024',
        ];

    }
    function messages()
    {
        return[
            'photo.required'=>'Please Enter Image',
        ];
    }
}