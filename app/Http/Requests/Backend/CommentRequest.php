<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'news_id'=>'required|integer',
            'name'=>'required|string',
            'email'=>'required|string',
            'message'=>'required|string',
            'website'=>'required|string',
        ];

    }
    function messages()
    {
        return[
//            'news_id.required'=>'Please Enter News Id',
//            'name.required'=>'Please Enter Name',
//            'email.required'=>'Please Enter Email',
//            'message.required'=>'Please Enter Message',
//            'website.required'=>'Please Enter Website',
            //'photo.required'=>'Please Enter Image',
        ];
    }
}
