<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\TagRequest;
use App\Model\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TagController extends Controller
{
    //  function __construct()
    // {
    //    $this->middleware('auth')
    // }

    public function index()
    {
        //fetch all data from tag model
        $data['tags'] = Tag::all();
        //send data to view using compact method
        return view('backend.tag.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.tag.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagRequest $request)
    {
        //dd($request->all());
        //add login user id to request object
        $request->request->add(['created_by' => Auth::user()->id]);
        //  dd($request->all());
        //insert into tag table using Tag model
        $tag = Tag::create($request->all());
        if ($tag) {
            $request->session()->flash('success_message', 'Tag Created Successfully');
            return redirect()->route('tag.index');
        } else {
            $request->session()->flash('error_message', 'Tag Creation Failed');
            return redirect()->route('tag.create');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['tags'] = Tag::find($id);
        $data['tag']  = Tag::where('id',$id)->get();
        return view('backend.tag.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['tags'] = Tag::find($id);
        return view('backend.tag.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(TagRequest $request, $id)
    {
        //dd($request->all());
        //add login user id to request object
        $request->request->add(['updated_by' => Auth::user()->id]);
        //  dd($request->all());
        //insert into tag table using Tag model
        $tag = Tag::find($id);
        if ($tag->update($request->all())) {
            $request->session()->flash('success_message', 'Tag Updated Successfully');

        } else {
            $request->session()->flash('error_message', 'Tag Updated Failed');

        }
        return redirect()->route('tag.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $tag = Tag::find($id);
        if ($tag->delete()) {
            $request->session()->flash('success_message', 'Tag Deleted Successfully');

        } else {
            $request->session()->flash('error_message', 'Tag Deleted Failed');

        }
        return redirect()->route('tag.index');
    }
}
