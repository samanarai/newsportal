<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\CommentRequest;
use App\Model\Comment;
use App\Model\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //fetch all data from Category Model
        $data['news']=News::all();
        $data['comments']=Comment::all();
        //send data to view using compact method
        return view('backend.comment.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['news']=News::pluck('title','id');
        return view('backend.comment.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request)
    {
        //dd($request->all());
        //add login user id to request object
        $request->request->add(['created_by' =>Auth::user()->id]);
        //  dd($request->all());
        //insert into category table using Category model
        $comment = Comment::create($request->all());
        if ($comment){
            $request->session()->flash('success_message','Comment Created Successfully');
            return redirect()->route('comment.index');
        }else{
            $request->session()->flash('error_message','Comment Creation Failed');
            return redirect()->route('comment.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['news']=News::pluck('title','id');
        $data['comments']=Comment::find($id);
        $data['comment']  = Comment::where('id',$id)->get();
        return view('backend.comment.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['news']=News::pluck('title','id');
        $data['comments']=Comment::find($id);
        return view('backend.comment.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CommentRequest $request, $id)
    {
        //dd($request->all());
        //add login user id to request object
        $request->request->add(['updated_by' =>Auth::user()->id]);
        //  dd($request->all());
        //insert into tag table using Tag model
        $comment = Comment::find($id);
        if ($comment->update($request->all())){
            $request->session()->flash('success_message','Comment Updated Successfully');

        }else{
            $request->session()->flash('error_message','Comment Updated Failed');

        }
        return redirect()->route('comment.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $comment = Comment::find($id);
        if ($comment->delete()) {
            $request->session()->flash('success_message', 'Comment Deleted Successfully');

        } else {
            $request->session()->flash('error_message', 'Comment Deleted Failed');

        }
        return redirect()->route('comment.index');
    }
}
