<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\AuthorRequest;
use App\Model\Author;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //fetch all data from Category Model
        $data['authors']=Author::all();
        //send data to view using compact method
        return view('backend.author.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.author.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AuthorRequest $request)
    {
        if(!empty($request->file('photo'))) {
            //dd($request->all());
            //add login user id to request object


            $author_image = $request->file('photo');

            $image_name = uniqid() . '.' . $author_image->getClientOriginalExtension();
            $destinationPath = public_path('/images/author');
            $author_image->move($destinationPath, $image_name);
            $request->request->add(['image' => $image_name]);
        }
        $request->request->add(['created_by' =>Auth::user()->id]);
        $author = Author::create($request->all());
        if ($author){
            $request->session()->flash('success_message','Author Created Successfully');
            return redirect()->route('author.index');
        }else{
            $request->session()->flash('error_message','Author Creation Failed');
            return redirect()->route('author.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['authors']=Author::find($id);
        $data['author']  = Author::where('id',$id)->get();
        return view('backend.author.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['authors']=Author::find($id);
        return view('backend.author.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AuthorRequest $request, $id)
    {
        // dd($request->all());
        //add login user id to request object
        $author = Author::find($id);
        if (!empty($request->file('photo'))) {
            $author_image = $request->file('photo');

            $image_name      = uniqid().'.'.$author_image->getClientOriginalName();
            $destinationPath = public_path('/images/author');
            $author_image->move($destinationPath, $image_name);
            $request->request->add(['image' => $image_name]);
            if (file_exists('images/author/' . $author->image)) {
                unlink('images/author/' . $author->image);
            }
        }

        $request->request->add(['updated_by' =>Auth::user()->id]);
        if ($author->update($request->all())){
            $request->session()->flash('success_message','Author Updated Successfully');

        }else{
            $request->session()->flash('error_message','Author Updated Failed');

        }
        return redirect()->route('author.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $author = Author::find($id);
        if ($author->delete()) {
            $request->session()->flash('success_message', 'Author Deleted Successfully');

        } else {
            $request->session()->flash('error_message', 'Author Deleted Failed');

        }
        $imagename = $author->image;
        if (!empty($imagename) && file_exists('images/author/' . $imagename)) {
            unlink('images/author/' . $imagename);
        }
        $author->delete();
        session()->flash('success', 'Sucessfully removed the post');

        return redirect()->route('author.index');
    }
}
