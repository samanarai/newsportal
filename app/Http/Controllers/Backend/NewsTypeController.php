<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\NewsTypeRequest;
use App\Model\News;
use App\Model\NewsType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NewsTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //fetch all data from Category Model
        $data['newstypes']=NewsType::all();
        //send data to view using compact method
        return view('backend.newstype.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.newstype.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsTypeRequest $request)
    {
        if(!empty($request->file('photo'))) {
            //dd($request->all());
            //add login user id to request object


            $newstype_image = $request->file('photo');

            $image_name = uniqid() . '.' . $newstype_image->getClientOriginalExtension();
            $destinationPath = public_path('/images/newstype');
            $newstype_image->move($destinationPath, $image_name);
            $request->request->add(['feature_image' => $image_name]);
        }
        $request->request->add(['created_by' =>Auth::user()->id]);
        $news = NewsType::create($request->all());
        if ($news){
            $request->session()->flash('success_message','NewsType Created Successfully');
            return redirect()->route('newstype.index');
        }else{
            $request->session()->flash('error_message','NewsType Creation Failed');
            return redirect()->route('newstype.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['newstypes']=NewsType::find($id);
        $data['newstype']  = NewsType::where('id',$id)->get();
        return view('backend.newstype.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['newstypes']=NewsType::find($id);
        return view('backend.newstype.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewsTypeRequest $request, $id)
    {
        //dd($request->all());
        //add login user id to request object
        $newstype = NewsType::find($id);
        if (!empty($request->file('photo'))) {
            $newstype_image = $request->file('photo');

            $image_name      = uniqid().'.'.$newstype_image->getClientOriginalName();
            $destinationPath = public_path('/images/newstype');
            $newstype_image->move($destinationPath, $image_name);
            $request->request->add(['feature_image' => $image_name]);
            if (file_exists('images/newstype/' . $newstype->feature_image)) {
                unlink('images/newstype/' . $newstype->feature_image);
            }
        }

        $request->request->add(['updated_by' =>Auth::user()->id]);
        $newstype = $newstype->update($request->all());
        if ($newstype) {
            $request->session()->flash('success_message','News Type Updated Successfully');

        }else{
            $request->session()->flash('error_message','News Type Updated Failed');

        }
        return redirect()->route('newstype.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $newstype = NewsType::find($id);
        if ($newstype->delete()) {
            $request->session()->flash('success_message', 'NewsType Deleted Successfully');

        } else {
            $request->session()->flash('error_message', 'NewsType Deleted Failed');

        }
        $imagename = $newstype->feature_image;
        if (!empty($imagename) && file_exists('images/newstype/' . $imagename)) {
            unlink('images/newstype/' . $imagename);
        }
        $newstype->delete();
        session()->flash('success', 'Sucessfully removed the post');
        return redirect()->route('newstype.index');
    }

}
