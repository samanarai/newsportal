<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\UserRequest;
use App\Model\Role;
use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['users']=User::all();
        $data['roles']=Role::all();
        //send data to view using compact method
        return view('backend.user.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['roles']=Role::pluck('name','id');
        return view('backend.user.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        if(!empty($request->file('photo'))) {
            //       dd($request->all());
            //add login user id to request object


            $user = $request->file('photo');

            $image_name = uniqid() . '.' . $user->getClientOriginalExtension();
            $destinationPath = public_path('/images/users');
            $user->move($destinationPath, $image_name);
            $request->request->add(['image' => $image_name]);
        }
        $request->request->add(['created_by' => Auth::user()->id]);
        //insert into user table using User model
        $user = User::create($request->all());

        //dd($request->all());
//        //add login user id to request object
//        $password=Hash::make('Password');

        //  dd($request->all());

        if ($user) {
            $request->session()->flash('success_message', 'User Created Successfully');
            return redirect()->route('user.index');
        } else {
            $request->session()->flash('error_message', 'User Creation Failed');
            return redirect()->route('user.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['users'] = User::find($id);
        $data['user']  = User::where('id',$id)->get();
        return view('backend.user.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['roles']=Role::pluck('name','id');
        $data['users'] = User::find($id);
        return view('backend.user.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $user = User::find($id);
        if (!empty($request->file('photo'))) {
            $user_image = $request->file('photo');
            $image_name = uniqid() . '.' . $user_image->getClientOriginalExtension();
            $destinationPath = public_path('/images/users');
            $user_image->move($destinationPath, $image_name);
            $request->request->add(['image' => $image_name]);
            //before uploading new image make sure to remove the old image
            if (file_exists('images/users/' . $user->image)) {
                unlink('images/users/' . $user->image);
            }
        }
        $request->request->add(['updated_by' => Auth::user()->id]);
        //save data
//        $user -> save();
        $user = $user->update($request->all());
        if ($user) {
            $request->session()->flash('success_message', 'User Update Success!!');
        } else {
            $request->session()->flash('error_message', 'User Update Failed!!');
        }
        return redirect()->route('user.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $user = User::find($id);
        if ($user->delete()) {
            $request->session()->flash('success_message', 'User Deleted Successfully');

        } else {
            $request->session()->flash('error_message', 'User Deleted Failed');

        }
        $imagename = $user->image;
        if (!empty($imagename) && file_exists('images/users/' . $imagename)) {
            unlink('images/users/' . $imagename);
        }
        $user->delete();
        session()->flash('success', 'Sucessfully removed the post');
        return redirect()->route('user.index');
    }

}
