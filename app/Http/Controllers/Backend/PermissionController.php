<?php

namespace App\Http\Controllers\backend;

use App\Http\Requests\Backend\PermissionRequest;
use App\Model\Module;
use App\Model\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //fetch all data from Category Model
        $data['permissions']=Permission::all();
         $data['modules']=Module::all();
        //send data to view using compact method

        return view('backend.permission.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['modules']=Module::pluck('name','id');
        return view('backend.permission.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermissionRequest $request)
    {
        //dd($request->all());
        //add login user id to request object
        $request->request->add(['created_by' =>Auth::user()->id]);



        //  dd($request->all());
        //insert into category table using Category model
        $permission =Permission::create($request->all());



        if ($permission) {
            $request->session()->flash('success_message', 'Slider Created Successfully');
            return redirect()->route('permission.index');
        } else {
            $request->session()->flash('error_message', 'Slider Creation Failed');
            return redirect()->route('permission.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['modules']=Module::pluck('name','id');
        $data['permissions']=Permission::find($id);
        $data['permission']  = Permission::where('id',$id)->get();
        return view('backend.permission.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['modules']=Module::pluck('name','id');
        $data['permissions']=Permission::find($id);
        return view('backend.permission.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        //add login user id to request object
        $request->request->add(['updated_by' => Auth::user()->id]);
        //  dd($request->all());
        //insert into tag table using Tag model
        $permission = Permission::find($id);
        if ($permission->update($request->all())) {
            $request->session()->flash('success_message', 'Permission Updated Successfully');

        } else {
            $request->session()->flash('error_message', 'Permission Updated Failed');

        }
        return redirect()->route('permission.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $permisson = Permission::find($id);
        if ($permisson->delete()) {
            $request->session()->flash('success_message', 'Permission Deleted Successfully');

        } else {
            $request->session()->flash('error_message', 'Permission Deleted Failed');

        }
        return redirect()->route('permission.index');

    }
}
