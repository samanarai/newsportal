<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\ModuleRequest;
use App\Model\Module;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //fetch all data from Category Model
        $data['modules']=Module::all();
        //send data to view using compact method
        return view('backend.module.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.module.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ModuleRequest $request)
    {
        //dd($request->all());
        //add login user id to request object
        $request->request->add(['created_by' => Auth::user()->id]);


        //  dd($request->all());
        //insert into category table using Category model
        $module = Module::create($request->all());


        if ($module) {
            $request->session()->flash('success_message', 'Slider Created Successfully');
            return redirect()->route('module.index');
        } else {
            $request->session()->flash('error_message', 'Slider Creation Failed');
            return redirect()->route('module.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['modules']=Module::find($id);
        $data['module']  = Module::where('id',$id)->get();
        return view('backend.module.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['modules']=Module::find($id);
        return view('backend.module.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ModuleRequest $request, $id)
    {
        //dd($request->all());
        //add login user id to request object
        $request->request->add(['updated_by' => Auth::user()->id]);
        //  dd($request->all());
        $module = Module::find($id);
        if ($module->update($request->all())) {
            $request->session()->flash('success_message', 'Module Updated Successfully');

        } else {
            $request->session()->flash('error_message', 'Module Updated Failed');

        }
        return redirect()->route('module.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $module = Module::find($id);
        if ($module->delete()) {
            $request->session()->flash('success_message', 'Module Deleted Successfully');

        } else {
            $request->session()->flash('error_message', 'Module Deleted Failed');

        }
        return redirect()->route('module.index');
    }
}
