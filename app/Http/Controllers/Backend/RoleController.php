<?php

namespace App\Http\Controllers\backend;

use App\Http\Requests\Backend\RoleRequest;
use App\Model\Module;
use App\Model\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //fetch all data from Category Model
        $data['roles'] = Role::all();
        //send data to view using compact method
        return view('backend.role.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        //dd($request->all());
        //add login user id to request object
        $request->request->add(['created_by' => Auth::user()->id]);


        //  dd($request->all());
        //insert into category table using Category model
        $role = Role::create($request->all());


        if ($role) {
            $request->session()->flash('success_message', 'Slider Created Successfully');
            return redirect()->route('role.index');
        } else {
            $request->session()->flash('error_message', 'Slider Creation Failed');
            return redirect()->route('role.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['roles'] = Role::find($id);
        $data['role']  = Role::where('id',$id)->get();
        return view('backend.role.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['roles'] = Role::find($id);
        return view('backend.role.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, $id)
    {
        //dd($request->all());
        //add login user id to request object
        $request->request->add(['updated_by' => Auth::user()->id]);
        //  dd($request->all());
        //insert into tag table using Tag model
        $role = Role::find($id);
        if ($role->update($request->all())) {
            $request->session()->flash('success_message', 'Role Updated Successfully');

        } else {
            $request->session()->flash('error_message', 'Role Updated Failed');

        }
        return redirect()->route('role.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $role = Role::find($id);
        if ($role->delete()) {
            $request->session()->flash('success_message', 'Role Deleted Successfully');

        } else {
            $request->session()->flash('error_message', 'Role Deleted Failed');

        }
        return redirect()->route('role.index');
    }


    public function assignPermission ($id)
    {
        $data['roles'] = Role::find($id);
        $data['modules'] = Module::all();
        $data['permissions'] = array_column($data['roles']->permissions()
            ->get()->toArray(),'id');
        return view('backend.role.assignpermission', compact('data'));
    }

    public function savePermission (Request $request,$id)
    {
        $data['roles'] = Role::find($id);
        if ($data['roles']->permissions()->sync($request->input('permission_id'))) {
            $request->session()->flash('success_message', 'Role Assigned Successfully');
        } else {
            $request->session()->flash('error_message', 'Role Assign Failed');
        }
        return redirect()->route('role.index');
    }


}