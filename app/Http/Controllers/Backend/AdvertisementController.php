<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\AdvertisementRequest;
use App\Model\Advertisement;
use App\Model\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdvertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //fetch all data from Category Model
        $data['advertisements']=Advertisement::all();
        //send data to view using compact method
        return view('backend.advertisement.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['pages'] = Page::pluck('page_name','id');
        return view('backend.advertisement.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdvertisementRequest $request)
    {

        if(!empty($request->file('photo'))) {
            //dd($request->all());
            //add login user id to request object


            $advertisement_image = $request->file('photo');

            $image_name = uniqid() . '.' . $advertisement_image->getClientOriginalExtension();
            $destinationPath = public_path('/images/advertisement');
            $advertisement_image->move($destinationPath, $image_name);
            $request->request->add(['image' => $image_name]);
        }
        $request->request->add(['created_by' =>Auth::user()->id]);
        $advertisement = Advertisement::create($request->all());
        if ($advertisement){
            $request->session()->flash('success_message','Advertisement Created Successfully');
            return redirect()->route('advertisement.index');
        }else{
            $request->session()->flash('error_message','Advertisement Creation Failed');
            return redirect()->route('advertisement.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['advertisements']=Advertisement::find($id);
        $data['advertisement']  = Advertisement::where('id',$id)->get();
        return view('backend.advertisement.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['pages'] = Page::pluck('page_name','id');
        $data['advertisements']=Advertisement::find($id);
        return view('backend.advertisement.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdvertisementRequest $request, $id)
    {
        //dd($request->all());
        //add login user id to request object
        $advertisement = Advertisement::find($id);
            if (!empty($request->file('photo'))) {
                $advertisement_image = $request->file('photo');
                $image_name = uniqid() . '.' . $advertisement_image->getClientOriginalExtension();
                $destinationPath = public_path('/images/advertisement');
                $advertisement_image->move($destinationPath, $image_name);
                $request->request->add(['image' => $image_name]);
                //before uploading new image make sure to remove the old image
                if (file_exists('images/advertisement/' . $advertisement->image)) {
                    unlink('images/advertisement/' . $advertisement->image);
                }
            }
            $request->request->add(['updated_by' =>Auth::user()->id]);

            $advertisement = $advertisement->update($request->all());
        if ($advertisement) {
            $request->session()->flash('success_message', 'Advertisement Update Success!!');
        } else {
            $request->session()->flash('error_message', 'Advertisement Update Failed!!');
        }
        return redirect()->route('advertisement.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $advertisement = Advertisement::find($id);
        if ($advertisement->delete()) {
            $request->session()->flash('success_message', 'Advertisement Deleted Successfully');

        } else {
            $request->session()->flash('error_message', 'Advertisement Deleted Failed');

        }
        $imagename = $advertisement->image;
        if (!empty($imagename) && file_exists('images/advertisement/' . $imagename)) {
            unlink('images/advertisement/' . $imagename);
        }
        $advertisement->delete();
        session()->flash('success', 'Successfully removed the post');
        return redirect()->route('advertisement.index');
    }
}
