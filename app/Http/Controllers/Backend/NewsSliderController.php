<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\NewsSliderRequest;
use App\Model\NewsSlider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NewsSliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //fetch all data from Category Model
        $data['news_sliders']=NewsSlider::all();
        //send data to view using compact method
        return view('backend.news_slider.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.news_slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsSliderRequest $request)
    {
        if(!empty($request->file('photo'))) {
     //       dd($request->all());
            //add login user id to request object


            $news_sliderimage = $request->file('photo');

            $image_name = uniqid() . '.' . $news_sliderimage->getClientOriginalExtension();
            $destinationPath = public_path('/images/newsslider');
            $news_sliderimage->move($destinationPath, $image_name);
            $request->request->add(['image' => $image_name]);
        }
        $request->request->add(['created_by' =>Auth::user()->id]);
        $news_slider = NewsSlider::create($request->all());
        if ($news_slider){
            $request->session()->flash('success_message','News Slider Created Successfully');
            return redirect()->route('news_slider.index');
        }else{
            $request->session()->flash('error_message','News Slider Creation Failed');
            return redirect()->route('news_slider.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['news_sliders']=NewsSlider::find($id);
        $data['news_slider']  = NewsSlider::where('id',$id)->get();
        return view('backend.news_slider.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['news_sliders']=NewsSlider::find($id);
        return view('backend.news_slider.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewsSliderRequest $request, $id)
    {
        $news_slider = NewsSlider::find($id);
        if (!empty($request->file('photo'))) {
            $news_slider_image = $request->file('photo');
            $image_name = uniqid() . '.' . $news_slider_image->getClientOriginalExtension();
            $destinationPath = public_path('/images/newsslider');
            $news_slider_image->move($destinationPath, $image_name);
            $request->request->add(['image' => $image_name]);
            //before uploading new image make sure to remove the old image
            if (file_exists('images/newsslider/' . $news_slider->image)) {
                unlink('images/newsslider/' . $news_slider->image);
            }
        }
        $request->request->add(['updated_by' => Auth::user()->id]);
        //save data
//        $news_slider -> save();
        $news_slider = $news_slider->update($request->all());
        if ($news_slider) {
            $request->session()->flash('success_message', 'NewsSlider Update Success!!');
        } else {
            $request->session()->flash('error_message', 'NewsSlider Update Failed!!');
        }
        return redirect()->route('news_slider.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {

        $news_slider = NewsSlider::find($id);
        if ($news_slider->delete()) {
            $request->session()->flash('success_message', 'News Slider Deleted Successfully');

        } else {
            $request->session()->flash('error_message', 'News Slider Deleted Failed');

        }
        $imagename = $news_slider->image;
        if (!empty($imagename) && file_exists('images/newsslider/' . $imagename)) {
            unlink('images/newsslider/' . $imagename);
        }
        $news_slider->delete();
        session()->flash('success', 'Sucessfully removed the post');
        return redirect()->route('news_slider.index');
    }

}
