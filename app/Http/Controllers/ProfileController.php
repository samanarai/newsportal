<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
class ProfileController extends Controller {

    public function index() {
        return view('profile');
    }
    public function profile(Request $request){
        $this->validate($request, [
            'old_password'=> 'required',
            'new_password' =>'required|min:8',
            'confirm_password' => 'required|min:8|same:new_password',
        ]);
        $oldPassword = $request->old_password;
        $newPassword = $request->new_password;
        if(!Hash::check($oldPassword, Auth::user()->password)){
            $request->session()->flash('error_message','The specified password does not match the database password');
            return redirect()->back();
//            return back()->flash('erroe_message','The specified password does not match the database password'); //when user enter wrong password as current password
        }else{
            $request->user()->fill(['password' => Hash::make($newPassword)])->save(); //updating password into user table
            $request->session()->flash('success_message', 'Password has been updated');
//            return back()->flash('success_message','Password has been updated');
            return redirect()->back();
        }
//         echo 'here update query for password';
    }


}

