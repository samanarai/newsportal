<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tasks = Task::orderBy('id','desc')->paginate(5);
        return view('home')->with('storedTasks', $tasks);
//        return view('home');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'newTaskName' => 'required|min:5|max:255',
        ]);
        $task = new Task;
        $task->name = $request->newTaskName;
        $task->save();
        Session::flash('success', 'New task has been succesfully added!');
        return redirect()->route('home');
    }
    public function destroy($id)
    {
        $task = Task::find($id);
        $task->delete();
        Session::flash('success', 'Task #' . $id . ' has been successfully deleted.');
        return redirect()->route('home');
    }
    public function profile()
    {
        return view('backend.profile');
    }
}
