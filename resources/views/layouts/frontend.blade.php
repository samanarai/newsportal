<!doctype html>
<html class="no-js" lang="en">


<!-- Mirrored from demo.hasthemes.com/khobor-preview/khobor-v1/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 25 Sep 2019 11:02:22 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Khobor - Modern Magazine & Newspaper HTML Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('frontend/img/favicon.ico')}}">

    <!-- CSS
	============================================ -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('/frontend/css/bootstrap.min.css')}}">
    <!-- Icon Font CSS -->
    <link rel="stylesheet" href="{{asset('/frontend/css/font-awesome.min.css')}}">
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="{{asset('/frontend/css/plugins.css')}}">
    <!-- ycp -->
    <link rel="stylesheet" href="{{asset('/frontend/css/ycp.css')}}">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{asset('/frontend/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('/frontend/css/animate.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('/frontend/plugins/fancybox/jquery.fancybox.css')}}" media="screen" />

{{--<link rel="stylesheet"  href="{{asset('frontend/css/fancybox/jquery.fancybox-1.3.4.css')}}" media="screen" />--}}
{{--<link rel="stylesheet" href="{{asset('frontend/css/fancybox/style.css')}}" />--}}

{{--<script type="text/javascript" src="{{asset('frontend/js/vendor/jquery-1.4.3.min.js')}}"></script>--}}


<!-- Modernizer JS -->


    <script src="{{asset('frontend/js/vendor/modernizr-2.8.3.min.js')}}"></script>


    @yield('css')
</head>

<body>

<!-- Main Wrapper -->
<div id="main-wrapper">

    <!-- Header Top Start -->
    <div class="header-top section">
        <div class="container">
            <div class="row">

                <!-- Header Top Links Start -->
                <div class="header-top-links col-md-9 col-6">

                    <!-- Header Links -->
                    <ul class="header-links">
                        <li class="disabled block d-none d-md-block"><a href="#"><i class="fa fa-clock-o"></i>{{  now()->format('l,Fj,Y ')}} </a></li>
                        {{--<li class="d-none d-md-block"><a href="#"><i class="fa fa-mixcloud"></i> <span class="weather-degrees">20 <span class="unit">c</span> </span> <span class="weather-location">- Sydney</span></a></li>--}}

                        <li><a href="{{route('frontend.contact')}}"><i class="fa fa-headphones"></i>Contact</a></li>

                    </ul>

                </div><!-- Header Top Links End -->

                <!-- Header Top Social Start -->
                <div class="header-top-social col-md-3 col-6">

                    <!-- Header Social -->
                    <div class="header-social">
                        <a href="{{$data['configurations']->fb_link}}"><i class="fa fa-facebook"></i></a>
                        <a href="{{$data['configurations']->twitter_link}}"><i class="fa fa-twitter"></i></a>
                        <a href="{{$data['configurations']->google_link}}"><i class="fa fa-google-plus"></i></a>
                        <a href="{{$data['configurations']->pinterest_link}}"><i class="fa fa-pinterest-p"></i></a>
                        <a href="{{$data['configurations']->youtube_link}}"><i class="fa fa-youtube-play"></i></a>
                        <a href="{{$data['configurations']->vimeo_link}}"><i class="fa fa-vimeo"></i></a>
                    </div>

                </div><!-- Header Top Social End -->

            </div>
        </div>
    </div><!-- Header Top End -->

    <!-- Header Start -->
    <div class="header-section section">
        <div class="container">
            <div class="row align-items-center">

                <!-- Header Logo -->
                <div class="header-logo col-md-4 d-none d-md-block">
                    <a href="{{route('frontend.index')}}" class="logo animated  swing delay-2s"><img src="{{asset('frontend/img/logo.png')}}" class="" alt="Logo"></a>
                </div>

                <!-- Header Banner -->
                <div class="header-banner col-md-8 col-12">
                    <div class="banner"><a href="#" class="logo animated  flipInX delay-2s"><img src="{{asset('images/advertisement/' . $data['advertisements'][0]->image)}}" alt="Header Banner"></a></div>
                </div>

            </div>
        </div>
    </div><!-- Header End -->

    <!-- Menu Section Start -->
    @include('frontend.front.category')
    @yield('content')

    <div class="instagram-section section">

        <div class="container-fluid">
            <div class="row">

                <!-- Full Width Instagram Carousel Start -->

                <div class="fullwidth-instagram-carousel instagram-carousel col pl-0 pr-0">

                    @foreach($data['galleries'] as $gallery)

                        <a  href="{{asset('images/gallery/' . $gallery->image)}}" class="fancybox-effects-c"><img src="{{asset('images/gallery/' . $gallery->image)}}" alt="instagram"  ></a>
                    @endforeach
                </div><!-- Full Width Instagram Carousel End -->


            </div>
        </div>
    </div><!-- Instagram Section End -->
    <!-- Footer Top Section Start -->
    <div class="footer-top-section section bg-dark">
        <div class="container-fluid">
            <div class="row">

                <!-- Footer Widget Start -->
                <div class="footer-widget col-xl-4 col-md-6 col-12 mb-60">

                    <!-- Title -->
                    <h4 class="widget-title">About Us</h4>

                    <div class="content fix">
                        <p>{{$data['pages']->description}}</p>

                        <!-- Footer Contact -->
                        <ol class="footer-contact">
                            <li><i class="fa fa-home"></i>{{$data['configurations']->address}}</li>
                            <li><i class="fa fa-envelope-open"></i>{{$data['configurations']->email}}</li>
                            <li><i class="fa fa-headphones"></i>{{$data['configurations']->phone}}</li>
                        </ol>

                        <!-- Footer Social -->
                        <div class="footer-social">
                            <a href="{{$data['configurations']->fb_link}}" class="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="{{$data['configurations']->twitter_link}}" class="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="{{$data['configurations']->insta_link}}" class="dribbble"><i class="fa fa-dribbble"></i></a>
                            <a href="{{$data['configurations']->google_link}}" class="google-plus"><i class="fa fa-google-plus"></i></a>
                        </div>

                    </div>


                </div><!-- Footer Widget End -->

                <!-- Footer Widget Start -->
                <div class="footer-widget col-xl-4 col-md-6 col-12 mb-60">

                    <!-- Title -->
                    <h4 class="widget-title">Popular News</h4>

                    <!-- Footer Widget Post Start -->
                    @foreach ($data['popular_news'] as $popular_news)
                        <div class="footer-widget-post">
                            <div class="post-wrap">

                                <!-- Image -->
                                <a href="{{route('frontend.postdetail',$popular_news->slug)}}" class="image"><img src="{{asset('images/news/' . $popular_news->feature_image)}}" alt="Post"></a>

                                <!-- Content -->
                                <div class="content">

                                    <!-- Title -->
                                    <h5 class="title"><a href="{{route('frontend.postdetail',$popular_news->slug)}}">{{$popular_news->title}}</a></h5>

                                    <!-- Meta -->
                                    <div class="meta fix">
                                        <span class="meta-item date"><i class="fa fa-clock-o"></i>10 March 2017</span>
                                    </div>

                                </div>

                            </div>
                        </div><!-- Footer Widget Post ENd -->

                    @endforeach
                    <a class="hvr-sweep-to-right" href="{{route('frontend.popular_news',$popular_news->id)}}">View all Popular News</a>

                </div><!-- Footer Widget End -->

                <!-- Footer Widget Start -->
                <div class="footer-widget col-xl-4 col-md-6 col-12 mb-60">

                    <!-- Title -->
                    <h4 class="widget-title">Top News</h4>
                @foreach ($data['top_news'] as $top_news)

                    <!-- Footer Widget Post Start -->
                        <div class="footer-widget-post">

                            <div class="post-wrap">

                                <!-- Image -->
                                <a href="{{route('frontend.postdetail',$top_news->slug)}}" class="image"><img src="{{asset('images/news/' . $top_news->feature_image)}}" alt="Post"></a>

                                <!-- Content -->
                                <div class="content">

                                    <!-- Title -->
                                    <h5 class="title"><a href="{{route('frontend.postdetail',$top_news->slug)}}">{{$top_news->title}}</a></h5>

                                    <!-- Meta -->
                                    <div class="meta fix">
                                        <span class="meta-item date"><i class="fa fa-clock-o"></i>10 March 2017</span>
                                    </div>

                                </div>

                            </div>
                        </div><!-- Footer Widget Post ENd -->
                    @endforeach
                    <a class="hvr-sweep-to-right" href="{{route('frontend.top_news',$top_news->id)}}">View all Top News</a>

                </div><!-- Footer Widget End -->

            </div>
        </div>
    </div><!-- Footer Top Section End -->

    <!-- Footer Bottom Section Start -->
    <div class="footer-bottom-section section bg-dark">
        <div class="container">
            <div class="row">

                <!-- Copyright Start -->
                <div class="copyright text-center col">
                    <p>Copyright © {{date('Y')}} Rights Reserved.</p>
                </div><!-- Copyright End -->

            </div>
        </div>
    </div><!-- Footer Bottom Section End -->


</div>


<!-- JS
============================================ -->

<!-- jQuery JS -->
<script src="{{asset('frontend/js/vendor/jquery-1.12.0.min.js')}}"></script>
<!-- Popper JS -->
<script src="{{asset('frontend/js/popper.min.js')}}"></script>
<!-- Bootstrap JS -->
<script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
<!-- Plugins JS -->
<script src="{{asset('frontend/js/plugins.js')}}"></script>
<!-- ycp JS -->
<script src="{{asset('frontend/js/ycp.js')}}"></script>
<!-- Ajax Mail JS -->
<script src="{{asset('frontend/js/ajax-mail.js')}}"></script>
<!-- Main JS -->
<script src="{{asset('frontend/js/main.js')}}"></script>

//fancybox

<script src="{{ asset('/frontend/plugins/fancybox/jquery.fancybox.js') }}"></script>
<script type="text/javascript">
    //        $("a.photo").fancybox({
    //            'transitionIn'   :  'elastic',
    //            'transitionOut'  :  'elastic',
    //            'speedIn'       :  600,
    //            'speedOut'      :  200,
    //            'closeClick' : true,
    //            'overlayShow'    :  false
    //        });
    $(function(){

        $(".fancybox-effects-c").fancybox({
            'transitionIn'   :  'elastic',
            'transitionOut'  :  'elastic',
            'speedIn'       :  600,
            'speedOut'      :  200,
            'closeClick' : 'true',

            openEffect : 'none',

            helpers : {
                title : {
                    type : 'inside'
                },
                overlay : {
                    css : {
                        'background' : 'rgba(238,238,238,0.85)'
                    }
                }
            }
        });


    });


</script>

@yield('js')
</body>


<!-- Mirrored from demo.hasthemes.com/khobor-preview/khobor-v1/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 25 Sep 2019 11:02:47 GMT -->
</html>
