<div class="form-group">
    {!!  Form::label('photo', 'Image'); !!}
    {!! Form::file('photo',null,['class' => 'form-control','id' => 'photo']); !!}
    @include('includes.single_field_validation',['field'=>'photo'])
    @if(isset($data['gallery']) && $data['gallery']->image)
        <img id="image" src="{{asset('images/gallery/' . $data['gallery']->image)}}" width="70" height="70">
                {!! Form::hidden('photo', $data['gallery']->image); !!}
        @endif
</div>


<div class="form-group">
    {!!  Form::label('status', 'Status'); !!}
    {!! Form::radio('status', '1') !!} Active
    {!! Form::radio('status', '0',true) !!} De Active
</div>