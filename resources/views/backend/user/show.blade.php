@extends('layouts.backend')
@section('title','User Show page')

@section('content')
    <section class="content-header">
        <h1>
            User Management
            <a href="{{route('user.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>
            <a href="{{route('user.index')}}" class="btn btn-info">
                <i class="fa fa-list"></i>
                List
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('user.index')}}">User</a></li>
            <li class="active">Show  page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Show Page</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('includes.flash')
               <table class="table table-bordered">
                   <thead>
                   <tr>
                       <th>Role Name</th>
                       <td>{{$data['users']->role->name}}</td>
                   </tr>
                   <tr>
                       <th>Name</th>
                       <td>{{$data['users']->name}}</td>
                   </tr>
                   <tr>
                       <th>Email</th>
                       <td>{{$data['users']->email}}</td>
                   </tr>
                   <tr>
                   <tr>
                       <th>Password</th>
                       <td>{{$data['users']->password}}</td>

                   </tr>
                   <tr>
                       <th>Address</th>
                       <td>{{$data['users']->address}}</td>

                   </tr>
                   <tr>
                       <th>Phone</th>
                       <td>{{$data['users']->phone}}</td>

                   </tr>
                   <tr>
                       <th>Description</th>
                       <td>{!!$data['users']->description  !!}</td>

                   </tr>
                   <tr>
                       <th>Image</th>
                       <td>
                           <img src="{{asset("images/users/".$data['users']->image)}}" alt=""></td>
                   </tr>
                   <tr>
                       <th>Created At</th>
                       <td>{{$data['users']->created_at->format('j F,Y')}}</td>

                   </tr>
                   <tr>
                       <th>Updated At</th>
                       <td>{{$data['users']->updated_at->format('j F,Y')}}</td>

                   </tr>
                   <tr>
                       @foreach($data['user'] as $user)
                           <td>
                               <a href="{{route('user.edit',$user->id)}}" class="btn btn-warning">
                                   <i class="fa fa-pencil"></i>
                                   Edit
                               </a>
                           </td>
                           <td>
                               <form action="{{route('user.destroy',$user->id)}}" method="post"
                                     onsubmit="return confirm('Are you sure?')">
                                   @csrf
                                   <input type="hidden" name="_method" value="DELETE"/>
                                   <button class="btn btn-danger"><i class="fa fa-trash"></i>Delete</button>
                               </form>
                           </td>
                       @endforeach
                   </tr>
                   </thead>
               </table>
            </div>

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection