<div class="form-group">
    <label for="role_id">Role id</label>
    {!! Form::select('role_id', $data['roles'],null,['class'=>'form-control']); !!}

</div>
<div class="form-group">

        {!!  Form::label('name', 'Name'); !!}
        {!! Form::text('name', null,['class' => 'form-control','id' => 'name','placeholder' => 'enter name']); !!}
        @include('includes.single_field_validation',['field'=>'name'])
</div>
<div class="form-group">

    {!!  Form::label('email', 'Email'); !!}
    {!! Form::text('email', null,['class' => 'form-control','id' => 'email','placeholder' => 'enter email']); !!}
    @include('includes.single_field_validation',['field'=>'email'])
</div>
<div class="form-group">
    {!!  Form::label('password', 'Password'); !!}
    {!! Form::password('password', null,['class' => 'form-control','id' => 'password','placeholder' => 'enter password']); !!}
        @include('includes.single_field_validation',['field'=>'password'])

</div>
<div class="form-group">
    {!!  Form::label('address', 'Address'); !!}
    {!! Form::text('address', null,['class' => 'form-control','id' => 'address','placeholder' => 'enter address']); !!}
    @include('includes.single_field_validation',['field'=>'address'])

</div>
<div class="form-group">
    {!!  Form::label('phone', 'Phone'); !!}
    {!! Form::number('phone', null,['class' => 'form-control','id' => 'phone','placeholder' => 'enter phone']); !!}
    @include('includes.single_field_validation',['field'=>'phone'])

</div>
<div class="form-group">
    {!!  Form::label('description', 'Description'); !!}
    {!! Form::textarea('description', null,['class' => 'form-control','id' => 'description','placeholder' => 'enter description']); !!}
    @include('includes.single_field_validation',['field'=>'description'])

</div>
<div class="form-group">
    {!!  Form::label('photo', 'Image'); !!}
    {!! Form::file('photo',null,['class' => 'form-control','id' => 'photo']); !!}
    @include('includes.single_field_validation',['field'=>'photo'])
    @if(isset($data['users']) && $data['users']->image)
        <img id="image" src="{{asset('images/users/' . $data['users']->image)}}" width="70" height="70">
        {!! Form::hidden('photo', $data['users']->image); !!}
    @endif
</div>
