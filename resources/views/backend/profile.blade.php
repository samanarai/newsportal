@extends('layouts.backend')
@section('content')
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-show-password/1.0.3/bootstrap-show-password.min.js"></script>
    <script type="text/javascript">
        $("#password").password('toggle');
    </script>
    ​
    <script src="https://cdn.rawgit.com/PascaleBeier/bootstrap-validate/v2.2.0/dist/bootstrap-validate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.min.js"></script>
    ​
    <script>
        $(document).ready(function() {
            $('#identicalForm').bootstrapValidator({
                feedbackIcons: {
//                        valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    password: {
                        validators: {
                            identical: {
                                field: 'confirm_password',
                                message: 'The password must be at least 8 characters'
                            }
                        }
                    },
                    confirm_password: {
                        validators: {
                            identical: {
                                field: 'password',
                                message: 'The password and its confirm are not the same'
                            }
                        }
                    }
                }
            });
        });
    </script>
@endsection
<div>
    <section class="content-header">
        <h1>Profile Management </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active">Profile</li>
        </ol>
    </section>
    ​
    ​
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Profile</h3>
                    </div>
                    <div class="col-md-6">
                        <!-- Profile Image -->
                        <div class="box box-primary">
                            <div class="box-body box-profile">
                                <img class="profile-user-img img-responsive img-circle" src="{{asset('images/users/' . \Illuminate\Support\Facades\Auth::user()->image)}}" alt="Admin">
                                <h3 class="profile-username text-center">{{\Illuminate\Support\Facades\Auth::user()->name}} </h3>
                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item">
                                        <b>User Type</b> <span class="pull-right"> {{App\Model\Role::find(\Illuminate\Support\Facades\Auth::user()->role_id)->name}} </span>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Email</b> <span class="pull-right">{{\Illuminate\Support\Facades\Auth::user()->email}}</span>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Address</b> <span class="pull-right">{{\Illuminate\Support\Facades\Auth::user()->address}}</span>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Phone</b> <span class="pull-right">{{\Illuminate\Support\Facades\Auth::user()->phone}}</span>
                                    </li>
                                    <li class="list-group-item">
                                        <b>About Me</b><br>
                                    </li>
                                    <li class="list-group-item">
                                        {!!\Illuminate\Support\Facades\Auth::user()->description!!}
                                    </li>
                                    ​
                                </ul>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                        ​
                        <!-- /.box -->
                    </div>
                    <div class="col-md-6">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                {{--<li class="active"><a href="#general" data-toggle="tab">Personal Information</a></li>--}}
                                <li class="active"><a href="#security" data-toggle="tab">Password Management</a></li>
                                ​
                            </ul>
                            <div class="tab-content">
                                {{--<div class="tab-pane active" id="general">--}}
                                {{--<div class="box-body">--}}
                                {{--<form method="POST" action="" accept-charset="UTF-8" enctype="multipart/form-data"><input name="_method" type="hidden" value="PUT"><input name="_token" type="hidden" value="SS9ICMAZ2bKQMIrAzMXDCkwIZP1af18koIGdrcLi">--}}
                                ​
                            {{--<div class="form-group">--}}
                            {{--<div class="form-group">--}}
                            {{--<label for="name">Name</label>--}}
                            {{--<input type="text" name="name" class="form-control" id="name" value="{{\Illuminate\Support\Facades\Auth::user()->name}}"/>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                            {{--<label for="address">Address</label>--}}
                            {{--<input type="text" name="address" class="form-control" id="address" value="{{\Illuminate\Support\Facades\Auth::user()->address}}"/>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                            {{--<label for="phone">Phone Number</label>--}}
                            {{--<input type="tel" name="phone" class="form-control" id="phone" value="{{\Illuminate\Support\Facades\Auth::user()->phone}}"/>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                            {{--<label for="file">Profile Image (160 * 160)  </label>--}}
                            {{--<input type="file" name="file" id="profile-img">--}}
                            {{--<img src="{{asset('images/user/' . \Illuminate\Support\Facades\Auth::user()->image)}}" id="profile-img-tag" width="200px" />--}}
                            {{--</div>--}}
                            {{--<div class="box-footer fboxm alterCase">--}}
                            {{--<button type="submit" class="btn btn-primary"><i class="fa fa-check icheck"></i>Submit</button>--}}
                            {{--<button type="reset" class="btn btn-warning"><i class="fa fa-undo icheck"></i>Reset</button>--}}
                            {{--</div>--}}
                            {{--</form>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            <!-- /.tab-pane -->
                                <div class="tab-pane active" id="security">
                                    <div class="box-body">
                                        @include('includes.flash')
                                        <form id="identicalForm" method="POST" action="{{route('home.profile')}}" accept-charset="UTF-8" enctype="multipart/form-data"><input name="_token" type="hidden" value="SS9ICMAZ2bKQMIrAzMXDCkwIZP1af18koIGdrcLi">
                                            @csrf
                                            <div class="form-group">
                                                <label for="old_password">Old Password</label>
                                                <input type="password" name="old_password" id="old-password" class="form-control" placeholder="Enter old Password" data-toggle="password"/>
                                                <span style="color:red">{{$errors->first('old_password')}}</span>
                                            </div>
                                            <div class="form-group">
                                                <label for="password">New Password</label>
                                                <input type="password" name="new_password" id="new_password" class="form-control" placeholder="Enter new Password" data-toggle="password"/>
                                                <span style="color:red">{{$errors->first('new_password')}}</span>
                                            </div>
                                            <div class="form-group">
                                                <label for="confirm_password">Confirm Password</label>
                                                <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Enter confirm Password" data-toggle="password"/>
                                                <span style="color:red">{{$errors->first('confirm_password')}}</span>
                                            </div>
                                            <div class="box-footer fboxm alterCase">
                                                <button type="submit" class="btn btn-success"><i class="fa fa-check icheck"></i>Submit</button>
                                                <button type="reset" class="btn btn-danger"><i class="fa fa-undo icheck"></i>Reset</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- /.nav-tabs-custom -->
                    </div>                    <!-- /.box-body -->
                    ​
                    <div class="box-footer fboxm">
                    </div>
                </div>
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
@endsection
<!-- /.content-wrapper -->
​
​
​
