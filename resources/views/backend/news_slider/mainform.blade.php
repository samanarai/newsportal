
<div class="form-group">
    {!!  Form::label('title', 'Title'); !!}
    {!! Form::text('title', null,['class' => 'form-control','id' => 'title','placeholder' => 'enter title']); !!}
    @include('includes.single_field_validation',['field'=>'title'])
</div>
<div class="form-group">
    {!!  Form::label('slug', 'Slug'); !!}
    {!! Form::text('slug', null,['class' => 'form-control','id' => 'slug','placeholder' => 'enter slug']); !!}

    @include('includes.single_field_validation',['field'=>'slug'])
</div>
<div class="form-group">
    {!!  Form::label('comment', 'Comment'); !!}
    {!! Form::text('comment', null,['class' => 'form-control','id' => 'comment','placeholder' => 'enter comment']); !!}
    @include('includes.single_field_validation',['field'=>'comment'])
</div>
<div class="form-group">
    {!!  Form::label('link', 'Link'); !!}
    {!! Form::text('link', null,['class' => 'form-control','id' => 'link','placeholder' => 'enter link']); !!}
    @include('includes.single_field_validation',['field'=>'link'])
</div>

<div class="form-group">
    {!!  Form::label('photo', 'Image'); !!}
        {!! Form::file('photo',null,['class' => 'form-control','id' => 'photo']); !!}

    @include('includes.single_field_validation',['field'=>'photo'])
    @if(isset($data['news_sliders']) && $data['news_sliders']->image)
        <img id="image" src="{{asset('images/newsslider/' . $data['news_sliders']->image)}}" width="70" height="70">
        {!! Form::hidden('photo', $data['news_sliders']->image); !!}
    @endif
</div>
<div class="form-group">
    {!!  Form::label('meta_keyword', 'Meta Keyword'); !!}
    {!! Form::textarea('meta_keyword', null,['class' => 'form-control','id' => 'meta_keyword','placeholder' => 'enter meta keyword']); !!}

    @include('includes.single_field_validation',['field'=>'meta_keyword'])
</div>
<div class="form-group">
    {!!  Form::label('meta_description', 'Meta Description'); !!}
    {!! Form::textarea('meta_description', null,['class' => 'form-control','id' => 'meta_description','placeholder' => 'enter meta description']); !!}

    @include('includes.single_field_validation',['field'=>'meta_description'])
</div>
<div class="form-group">
    {!!  Form::label('short_description', 'Short Description'); !!}
    {!! Form::textarea('short_description', null,['class' => 'form-control','id' => 'short_description','placeholder' => 'enter short description']); !!}

    @include('includes.single_field_validation',['field'=>'short_description'])
</div>
<div class="form-group">
    {!!  Form::label('description', ' Description'); !!}
    {!! Form::textarea('description', null,['class' => 'form-control','id' => 'description','placeholder' => 'enter description']); !!}

    @include('includes.single_field_validation',['field'=>'description'])
</div>

<div class="form-group">
    {!!  Form::label('status', 'Status'); !!}
    {!! Form::radio('status', '1') !!} Active
    {!! Form::radio('status', '0',true) !!} De Active
</div>