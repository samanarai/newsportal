@extends('layouts.backend')
@section('title','News Slider Show page')

@section('content')
    <section class="content-header">
        <h1>
            NewsSlider Management
            <a href="{{route('news_slider.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>

            <a href="{{route('news_slider.index')}}" class="btn btn-info">
                <i class="fa fa-list"></i>
                List
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('news_slider.index')}}">NewsSlider</a></li>
            <li class="active">Show page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Show page</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('includes.flash')
               <table class="table table-bordered">
                   <thead>
                   <tr>
                       <th>Title</th>
                       <td>{{$data['news_sliders']->title}}</td>

                   </tr>
                   <tr>
                       <th>Slug</th>
                       <td>{{$data['news_sliders']->slug}}</td>
                   </tr>
                   <tr>
                       <th>Comment</th>
                       <td>{{$data['news_sliders']->comment}}</td>
                   </tr>
                   <tr>
                       <th>Image</th>
                       <td>
                           <img src="{{asset("images/newsslider/".$data['news_sliders']->image)}}" alt=""></td>
                   </tr>
                   <tr>
                       <th>Link</th>
                       <td>{{$data['news_sliders']->link}}</td>
                   </tr>
                   <tr>
                       <th>Description</th>
                       <td>{!! $data['news_sliders']->description !!}</td>
                   </tr>
                   <tr>
                       <th>Short Description</th>
                       <td>{{$data['news_sliders']->short_description}}</td>
                   </tr>
                   <tr>
                       <th>Meta Keyword</th>
                       <td>{{$data['news_sliders']->meta_keyword}}</td>
                   </tr>
                   <tr>
                       <th>Meta Description</th>
                       <td>{{$data['news_sliders']->meta_description}}</td>
                   </tr>

                   <tr>
                       <th>Status</th>
                       <td>
                           @if($data['news_sliders']->status==1)
                               <span class="label label-success">Active</span>
                           @else
                               <span class="label label-danger">Deactive</span>
                           @endif
                       </td>
                   </tr>
                   <tr>
                       <th>Created By</th>
                       <td>{{App\User::find($data['news_sliders']->created_by)->name}}</td>
                   </tr>
                   <tr>
                       <th>Created At</th>
                       <td>{{$data['news_sliders']->created_at->format('j F,Y')}}</td>
                   </tr>
                   <tr>
                       <th>Updated At</th>
                       <td>{{$data['news_sliders']->updated_at->format('j F,Y')}}</td>
                   </tr>
                   <tr>
                       <th>Updated By</th>
                       <td>@if(!empty($data['news_sliders']->updated_by))
                       {{App\User::find($data['news_sliders']->updated_by)->name}}
                       @endif </td>
                   </tr>
                   <tr>
                       @foreach($data['news_slider'] as $news_slider)
                           <td>
                               <a href="{{route('news_slider.edit',$news_slider->id)}}" class="btn btn-warning">
                                   <i class="fa fa-pencil"></i>
                                   Edit
                               </a>
                           </td>
                       <td>
                               <form action="{{route('news_slider.destroy',$news_slider->id)}}" method="post"
                                     onsubmit="return confirm('Are you sure?')">
                                   @csrf
                                   <input type="hidden" name="_method" value="DELETE"/>
                                   <button class="btn btn-danger"><i class="fa fa-trash"></i>Delete</button>
                               </form>
                           </td>
                       @endforeach
                   </tr>
                   </thead>
               </table>
            </div>

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection