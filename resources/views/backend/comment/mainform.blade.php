<div class="form-group">
    <label for="news_id">News id</label>
    {!! Form::select('news_id',$data['news'], null,['class' => 'form-control','id' => 'comment_id']); !!}
</div>

<div class="form-group">
    {!!  Form::label('name', 'Name'); !!}
    {!! Form::text('name', null,['class' => 'form-control','id' => 'name','placeholder' => 'enter name']); !!}
    @include('includes.single_field_validation',['field'=>'name'])
</div>

<div class="form-group">
    {!!  Form::label('email', 'Email'); !!}
    {!! Form::text('email', null,['class' => 'form-control','id' => 'email','placeholder' => 'enter email']); !!}

    @include('includes.single_field_validation',['field'=>'email'])
</div>
<div class="form-group">
    {!!  Form::label('website', 'Website'); !!}
    {!! Form::text('website', null,['class' => 'form-control','id' => 'website','placeholder' => 'enter website']); !!}
    @include('includes.single_field_validation',['field'=>'website'])
</div>
<div class="form-group">
        {!!  Form::label('message', 'Message'); !!}
    {!! Form::textarea('message', null,['class' => 'form-control','id' => 'message','placeholder' => 'enter message']); !!}

    @include('includes.single_field_validation',['field'=>'message'])
</div>

<div class="form-group">
    {!!  Form::label('status', 'Status'); !!}
    {!! Form::radio('status', '1') !!} Active
    {!! Form::radio('status', '0',true) !!} De Active
</div>