@extends('layouts.backend')
@section('title','Comment Index page')
@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Comment Management
            <a href="{{route('comment.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('comment.create')}}">Comment</a></li>
            <li class="active">Index page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Index page</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
               @include('includes.flash')
                <table class="table table-bordered" id="datatable">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>News Title</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    <tbody>
                    @php($i=1)

                    @foreach($data['comments'] as $comment)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$comment->news->title}}</td>
                            <td>{{$comment->name}}</td>
                            <td>{{$comment->email}}</td>
                            <td>
                                @if($comment->status==1)
                                    <span class="label label-success">Active</span>
                                @else
                                    <span class="label label-danger">Deactive</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{route('comment.show',$comment->id)}}" class="btn btn-info">
                                    <i class="fa fa-eye"></i>
                                    view
                                </a>

                                <a href="{{route('comment.edit',$comment->id)}}" class="btn btn-warning">
                                    <i class="fa fa-pencil"></i>
                                   Edit
                                </a>
                                <form action="{{route('comment.destroy',$comment->id)}}" method="post" onsubmit="return confirm('are you sure?')">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE"/>
                                    <button class="btn btn-danger"><i class="fa fa-trash"></i>Delete</button>
                                </form>
                                {{--<a href="{{route('frontend.postdetail',$comment->id)}}" class="btn btn-info">View Frontend</a>--}}
                                </td>
                        </tr>
                    @endforeach
                    </tbody>

                    </thead>
                </table>
            </div>

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
@section('js')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready( function () {
            $('#datatable').DataTable();
        } );
    </script>
@endsection