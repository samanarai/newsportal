<div class="form-group">
    {!!  Form::label('name', 'Name'); !!}
    {!! Form::text('name', null,['class' => 'form-control','id' => 'name','placeholder' => 'Enter Name']); !!}
    @include('includes.single_field_validation',['field'=>'name'])
</div>
<div class="form-group">
    {!!  Form::label('editor', 'Editor'); !!}
    {!! Form::text('editor', null,['class' => 'form-control','id' => 'editor','placeholder' => 'Enter Editor']); !!}
    @include('includes.single_field_validation',['field'=>'editor'])
</div>
<div class="form-group">
    {!!  Form::label('sub_editor', 'Sub Editor'); !!}
    {!! Form::text('sub_editor', null,['class' => 'form-control','id' => 'sub_editor','placeholder' => 'Enter Sub Editor']); !!}
    @include('includes.single_field_validation',['field'=>'sub_editor'])
</div>
<div class="form-group">
    {!!  Form::label('email', 'Email'); !!}
    {!! Form::text('email', null,['class' => 'form-control','id' => 'email','placeholder' => 'Enter Email']); !!}
    @include('includes.single_field_validation',['field'=>'email'])
</div>
<div class="form-group">
    {!!  Form::label('phone', 'Phone'); !!}
    {!! Form::text('phone', null,['class' => 'form-control','id' => 'phone','placeholder' => 'Enter Phone']); !!}
    @include('includes.single_field_validation',['field'=>'phone'])
</div>
<div class="form-group">
    {!!  Form::label('website', 'Website'); !!}
    {!! Form::text('website', null,['class' => 'form-control','id' => 'website','placeholder' => 'Enter Website']); !!}
    @include('includes.single_field_validation',['field'=>'website'])
</div>
<div class="form-group">
    {!!  Form::label('address', 'Address'); !!}
    {!! Form::text('address', null,['class' => 'form-control','id' => 'address','placeholder' => 'Enter Address']); !!}
    @include('includes.single_field_validation',['field'=>'address'])
</div>
<div class="form-group">
    {!!  Form::label('fav_icon', 'Fav Icon'); !!}
    {!! Form::file('fav_icon', null,['class' => 'form-control','id' => 'fav_icon','placeholder' => 'Enter Fav Icon']); !!}
    @include('includes.single_field_validation',['field'=>'fav_icon'])

</div>

<div class="form-group">
    {!!  Form::label('fb_link', 'Fb Link'); !!}
    {!! Form::text('fb_link', null,['class' => 'form-control','id' => 'fb_link','placeholder' => 'Enter Fb Link']); !!}

    @include('includes.single_field_validation',['field'=>'fb_link'])
</div>
<div class="form-group">
    {!!  Form::label('twitter_link', 'Twitter Link'); !!}
    {!! Form::text('twitter_link', null,['class' => 'form-control','id' => 'twitter_link','placeholder' => 'Enter Twitter Link']); !!}

    @include('includes.single_field_validation',['field'=>'twitter_link'])
</div>
<div class="form-group">
    {!!  Form::label('insta_link', 'Insta Link'); !!}
    {!! Form::text('insta_link', null,['class' => 'form-control','id' => 'insta_link','placeholder' => 'Enter Insta Link']); !!}

    @include('includes.single_field_validation',['field'=>'insta_link'])
</div>
<div class="form-group">
    {!!  Form::label('youtube_link', 'Youtube Link'); !!}
    {!! Form::text('youtube_link', null,['class' => 'form-control','id' => 'youtube_link','placeholder' => 'Enter Youtube Link']); !!}

    @include('includes.single_field_validation',['field'=>'youtube_link'])
</div>
<div class="form-group">
    {!!  Form::label('pinterest_link', 'Pinterest Link'); !!}
    {!! Form::text('pinterest_link', null,['class' => 'form-control','id' => 'pinterest_link','placeholder' => 'Enter Pinterest Link']); !!}

    @include('includes.single_field_validation',['field'=>'pinterest_link'])
</div>
<div class="form-group">
    {!!  Form::label('vimeo_link', 'Vimeo Link'); !!}
    {!! Form::text('vimeo_link', null,['class' => 'form-control','id' => 'vimeo_link','placeholder' => 'Enter Vimeo Link']); !!}

    @include('includes.single_field_validation',['field'=>'vimeo_link'])
</div>
<div class="form-group">
    {!!  Form::label('google_link', 'Google Link'); !!}
    {!! Form::text('google_link', null,['class' => 'form-control','id' => 'google_link','placeholder' => 'Enter Google Link']); !!}

    @include('includes.single_field_validation',['field'=>'youtube_link'])
</div>

<div class="form-group">
    {!!  Form::label('status', 'Status'); !!}
    {!! Form::radio('status', '1') !!} Active
    {!! Form::radio('status', '0',true) !!} De Active
</div>