<div class="form-group">
    {!!  Form::label('name', 'Name'); !!}
    {!! Form::text('name', null,['class' => 'form-control','id' => 'name','placeholder' => 'enter name']); !!}
    @include('includes.single_field_validation',['field'=>'name'])
</div>

<div class="form-group">
    {!!  Form::label('slug', 'Slug'); !!}
    {!! Form::text('slug', null,['class' => 'form-control','id' => 'slug','placeholder' => 'enter slug']); !!}

    @include('includes.single_field_validation',['field'=>'slug'])
</div>


<div class="form-group">
        {!!  Form::label('photo', 'Feature Image'); !!}
        {!! Form::file('photo',null,['class' => 'form-control','id' => 'photo']); !!}

        @include('includes.single_field_validation',['field'=>'photo'])
        @if(isset($data['newstypes']) && $data['newstypes']->feature_image)
                <img id="image" src="{{asset('images/newstype/' . $data['newstypes']->feature_image)}}" width="70" height="70">
                {!! Form::hidden('photo', $data['newstypes']->feature_image); !!}
        @endif
</div>


<div class="form-group">
    {!!  Form::label('status', 'Status'); !!}
    {!! Form::radio('status', '1') !!} Active
    {!! Form::radio('status', '0',true) !!} De Active
</div>