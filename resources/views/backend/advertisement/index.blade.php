@extends('layouts.backend')
@section('title','Advertisement Index page')
@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="{{asset('/frontend/plugins/fancybox/jquery.fancybox.css')}}" media="screen" />
@endsection
@section('js')
    <script src="{{ asset('/frontend/plugins/fancybox/jquery.fancybox.js') }}"></script>
    <script type="text/javascript">
        //        $("a.photo").fancybox({
        //            'transitionIn'   :  'elastic',
        //            'transitionOut'  :  'elastic',
        //            'speedIn'       :  600,
        //            'speedOut'      :  200,
        //            'closeClick' : true,
        //            'overlayShow'    :  false
        //        });
        $(function(){

            $(".fancybox-effects-c").fancybox({
                'transitionIn'   :  'elastic',
                'transitionOut'  :  'elastic',
                'speedIn'       :  600,
                'speedOut'      :  200,
                'closeClick' : 'true',

                openEffect : 'none',

                helpers : {
                    title : {
                        type : 'inside'
                    },
                    overlay : {
                        css : {
                            'background' : 'rgba(238,238,238,0.85)'
                        }
                    }
                }
            });


        });


    </script>
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Advertisements Management
            <a href="{{route('advertisement.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('advertisement.create')}}">Advertisement</a></li>
            <li class="active">Index page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Index Page</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
               @include('includes.flash')
                <table class="table table-bordered" id="datatable">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Page Name</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    <tbody>
                    @php($i=1)

                    @foreach($data['advertisements'] as $advertisement)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$advertisement->page->page_name}}</td>
                            <td>{{$advertisement->title}}</td>
                            <td>
                                <a href="{{ asset('images/advertisement/'.$advertisement->image) }}" class="fancybox-effects-c">
                                    <img src="{{ asset('images/advertisement/'.$advertisement->image) }}" width="100px">
                                </a>
                            </td>
                            <td>{{$advertisement->description}}</td>
                            <td>
                                @if($advertisement->status==1)
                                    <span class="label label-success">Active</span>
                                @else
                                    <span class="label label-danger">Deactive</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{route('advertisement.show',$advertisement->id)}}" class="btn btn-info">
                                    <i class="fa fa-eye"></i>
                                    view
                                </a>

                                <a href="{{route('advertisement.edit',$advertisement->id)}}" class="btn btn-warning">
                                    <i class="fa fa-pencil"></i>
                                   Edit
                                </a>
                                <form action="{{route('advertisement.destroy',$advertisement->id)}}" method="post" onsubmit="return confirm('are you sure?')">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE"/>
                                    <button class="btn btn-danger"><i class="fa fa-trash"></i>Delete</button>
                                </form>
                              </td>
                        </tr>
                    @endforeach
                    </tbody>

                    </thead>
                </table>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
@section('js')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready( function () {
            $('#datatable').DataTable();
        } );
    </script>
@endsection