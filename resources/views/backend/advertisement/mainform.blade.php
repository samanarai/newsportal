<div class="form-group">
    <label for="page_id">Page_id</label>
    {!! Form::select('page_id', $data['pages'],null,['class' => 'form-control']); !!}

</div>

<div class="form-group">
    {!!  Form::label('title', 'Title'); !!}
    {!! Form::text('title', null,['class' => 'form-control','id' => 'title','placeholder' => 'enter title']); !!}
    @include('includes.single_field_validation',['field'=>'title'])
</div>

<div class="form-group">
    {!!  Form::label('rank', 'Rank'); !!}
    {!! Form::number('rank', null,['class' => 'form-control','id' => 'rank','placeholder' => 'enter rank']); !!}

    @include('includes.single_field_validation',['field'=>'rank'])
</div>
<div class="form-group">
    {!!  Form::label('expired_at', 'Expired_At'); !!}
    {!! Form::date('expired_at', null,['class' => 'form-control','id' => 'expired_at']); !!}

    @include('includes.single_field_validation',['field'=>'expired_at'])
</div>
<div class="form-group">
    {!!  Form::label('photo', ' Image'); !!}
    {!! Form::file('photo',null,['class' => 'form-control','id' => 'photo']); !!}
    @include('includes.single_field_validation',['field'=>'photo'])
    @if(isset($data['advertisements']) && $data['advertisements']->image)
        <img src="{{asset('images/advertisement/' . $data['advertisements']->image)}}" width="600" height="210">
        {!! Form::hidden('photo', $data['advertisements']->image); !!}
    @endif
</div>

<div class="form-group">
    {!!  Form::label('description', ' Description'); !!}
    {!! Form::textarea('description', null,['class' => 'form-control','id' => 'description','placeholder' => 'enter description']); !!}

    @include('includes.single_field_validation',['field'=>'description'])
</div>

<div class="form-group">
    {!!  Form::label('status', 'Status'); !!}
    {!! Form::radio('status', '1') !!} Active
    {!! Form::radio('status', '0',true) !!} De Active
</div>