<div class="form-group">
    {!!  Form::label('name', 'Name'); !!}
    {!! Form::text('name', null,['class' => 'form-control','id' => 'name','placeholder' => 'enter name']); !!}
    @include('includes.single_field_validation',['field'=>'name'])
</div>
<div class="form-group">
    {!!  Form::label('rank', 'Rank'); !!}
    {!! Form::number('rank', null,['class' => 'form-control','id' => 'rank','placeholder' => 'enter rank']); !!}

    @include('includes.single_field_validation',['field'=>'rank'])
</div>
<div class="form-group">
    {!!  Form::label('slug', 'Slug'); !!}
    {!! Form::text('slug', null,['class' => 'form-control','id' => 'slug','placeholder' => 'enter slug']); !!}

    @include('includes.single_field_validation',['field'=>'slug'])
</div>
<div class="form-group">
    {!!  Form::label('photo', 'Feature Image'); !!}
        {!! Form::file('photo',null,['class' => 'form-control','id' => 'photo']); !!}
    @include('includes.single_field_validation',['field'=>'photo'])
    @if(isset($data['categories']) && $data['categories']->feature_image)
        <img src="{{asset('images/category/' . $data['categories']->feature_image)}}" width="70" height="70">
                {!! Form::hidden('photo', $data['categories']->feature_image); !!}
        @endif
</div>
<div class="form-group">
    {!!  Form::label('meta_keyword', 'Meta Keyword'); !!}
    {!! Form::textarea('meta_keyword', null,['class' => 'form-control','id' => 'meta_keyword','placeholder' => 'enter meta keyword']); !!}

    @include('includes.single_field_validation',['field'=>'meta_keyword'])
</div>
<div class="form-group">
    {!!  Form::label('meta_description', 'Meta Description'); !!}
    {!! Form::textarea('meta_description', null,['class' => 'form-control','id' => 'meta_description','placeholder' => 'enter meta description']); !!}

    @include('includes.single_field_validation',['field'=>'meta_description'])
</div>
<div class="form-group">
    {!!  Form::label('description', ' Description'); !!}
    {!! Form::textarea('description', null,['class' => 'form-control','id' => 'description','placeholder' => 'enter description']); !!}

    @include('includes.single_field_validation',['field'=>'description'])
</div>
<div class="form-group">
    {!!  Form::label('meta_title', 'Meta Title'); !!}
    {!! Form::textarea('meta_title', null,['class' => 'form-control','id' => 'meta_title','placeholder' => 'enter meta title']); !!}

    @include('includes.single_field_validation',['field'=>'meta_title'])
</div>
<div class="form-group">
    {!!  Form::label('status', 'Status'); !!}
    {!! Form::radio('status', '1') !!} Active
    {!! Form::radio('status', '0',true) !!} De Active
</div>