<div class="form-group">
    {!!  Form::label('url', 'URL'); !!}
    {!! Form::text('url', null,['class' => 'form-control','id' => 'url','placeholder' => 'Enter URL']); !!}
    @include('includes.single_field_validation',['field'=>'url'])
</div>
</div>
<div class="form-group">
    {!!  Form::label('status', 'Status'); !!}
    {!! Form::radio('status', '1') !!} Active
    {!! Form::radio('status', '0',true) !!} De Active
</div>