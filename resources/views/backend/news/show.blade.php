@extends('layouts.backend')
@section('title','News View page')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header" xmlns="http://www.w3.org/1999/html">
        <h1>
            News Management
            <a href="{{route('news.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>

            <a href="{{route('news.index')}}" class="btn btn-info">
                <i class="fa fa-list"></i>
                List
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">News</a></li>
            <li class="active">Show page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">


                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('includes.flash')
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Category Name</th>
                        <td>{{$data['news']->category->name}}</td>
                    </tr>
                    <tr>
                        <th>Title</th>
                        <td>{{$data['news']->title}}</td>
                    </tr>
                    <tr>
                        <th>Slug</th>
                        <td>{{$data['news']->slug}}</td>
                    </tr>
                    <tr>
                        <th>Feature Image</th>
                        <td>
                            <img src="{{asset("images/news/".$data['news']->feature_image)}}" alt=""></td>
                        </td>
                    </tr>
                    <tr>
                        <th>Short Description</th>
                        <td>{{$data['news']->short_description}}</td>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <td>{!! $data['news']->description !!}</td>
                    </tr>
                    <tr>
                        <th>MetaKeyword</th>
                        <td>{{$data['news']->meta_keyword}}</td>
                    </tr>
                    <tr>
                        <th>Meta Description</th>
                        <td>{{$data['news']->meta_description}}</td>
                    </tr>
                    <tr>
                        <th>Tags</th>
                        <td>
                            <ul>
                                @foreach($data['news']->tags as $tag)
                                    <li>{{$tag->name}}</li>
                                    @endforeach
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <th>Author</th>
                        <td>{{$data['news']->author->name}}</td>
                    </tr>
                    <tr>
                        <th>ViewsCount</th>
                        <td>{{$data['news']->viewscount}}</td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td>
                            @if($data['news']->status == 1)
                                <span class="label label-success">Active</span>
                            @else
                                <span class="label label-danger">De Active</span>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Created By</th>
                        <td>{{App\User::find($data['news']->created_by)->name}}
                        </td>

                    </tr>
                    <tr>
                        <th>Created At</th>
                        <td>{{$data['news']->created_at->format('j F,Y')}}
                        </td>
                    </tr>
                    <tr>
                        <th>Updated At</th>
                        <td>{{$data['news']->updated_at->format('j F,Y')}}
                        </td>
                    </tr>
                    <tr>
                        <th>Updated By</th>
                        <td>@if(!empty($data['news']->updated_by))
                                {{App\User::find($data['news']->updated_by)->name}}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        @foreach($data['new'] as $news)
                            <td>
                                <a href="{{route('news.edit',$news->id)}}" class="btn btn-warning">
                                    <i class="fa fa-pencil"></i>
                                    Edit
                                </a>
                            </td>
                            <td>
                                <form action="{{route('news.destroy',$news->id)}}" method="post"
                                      onsubmit="return confirm('Are you sure?')">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE"/>
                                    <button class="btn btn-danger"><i class="fa fa-trash"></i>Delete</button>
                                </form>
                            </td>
                        @endforeach
                    </tr>
                    </thead>
                </table>
                {{--<div class="row">--}}
                    {{--{{$data['category']->image}}--}}
                    {{--<div class="col-md-3">--}}
                        {{--<div class="img-container">--}}
                            {{--<button class="btn btn-danger btn-close">X</button>--}}
                            {{--<img src="{{asset('images/news/' .$data['news']->feature_image)}}" alt="" height="100" width="100">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            <!-- /.box-body -->

            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection