@extends('layouts.backend')
@section('content')
    <div class="box-body">
        <table class="table table-stripped">
            <thead>
            <tr>
                <th>SN</th>
                <th>Slug</th>
                <th>Title</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @php($i=1)
            @foreach($data['news'] as $news)
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$news->slug}}</td>
                    <td>{{$news->title}}</td>
                    <td>
                        <a href="{{route('news.show',$news->id)}}" class="btn btn-info">
                            <i class="fa fa-eye"></i>
                            View
                        </a>
                        <a href="{{route('news.edit',$news->id)}}" class="btn btn-warning">
                            <i class="fa fa-pencil"></i>
                            Edit
                        </a>
                        <form action="{{route('news.destroy',$news->id)}}" method="post"
                              onsubmit="return confirm('Are you sure?')">
                            @csrf
                            <input type="hidden" name="_method" value="DELETE"/>
                            <button class="btn-danger"><i class="fa fa-trash"></i>Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection