@extends('layouts.backend')
@section('title','News Index page')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('/frontend/plugins/fancybox/jquery.fancybox.css')}}" media="screen" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
@endsection
@section('js')
    <script src="{{ asset('/frontend/plugins/fancybox/jquery.fancybox.js') }}"></script>
    <script type="text/javascript">
        //        $("a.photo").fancybox({
        //            'transitionIn'   :  'elastic',
        //            'transitionOut'  :  'elastic',
        //            'speedIn'       :  600,
        //            'speedOut'      :  200,
        //            'closeClick' : true,
        //            'overlayShow'    :  false
        //        });
        $(function(){

            $(".fancybox-effects-c").fancybox({
                'transitionIn'   :  'elastic',
                'transitionOut'  :  'elastic',
                'speedIn'       :  600,
                'speedOut'      :  200,
                'closeClick' : 'true',

                openEffect : 'none',

                helpers : {
                    title : {
                        type : 'inside'
                    },
                    overlay : {
                        css : {
                            'background' : 'rgba(238,238,238,0.85)'
                        }
                    }
                }
            });


        });


    </script>
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            News Management
            <a href="{{route('news.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">News</a></li>
            <li class="active">View page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">


                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('includes.flash')
                <table class="table table-bordered" id="datatable">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Title</th>
                        <th>Feature Image</th>
                        <th>Meta Keyword</th>
                        <th>ViewsCount</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($i=1)
                    @foreach($data['news'] as $news)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$news->title}}</td>
                            <td>
                                <a href="{{ asset('images/news/'.$news->feature_image) }}" class="fancybox-effects-c">
                                    <img src="{{ asset('images/news/'.$news->feature_image) }}" width="100px">
                                </a>
                            </td>
                            <td>{{$news->meta_keyword}}</td>
                            <td>{{$news->viewscount}}</td>
                            <td>
                                @if($news->status == 1)
                                    <span class="label label-success">Active</span>
                                @else
                                    <span class="label label-danger">De Active</span>
                                @endif
                            </td>

                            <td>
                                <a href="{{route('news.show',$news->id)}}"
                                   class="btn btn-info">
                                    <i class="fa fa-eye"></i>
                                    View
                                </a>
                                <a href="{{route('news.edit',$news->id)}}"
                                   class="btn btn-warning">
                                    <i class="fa fa-pencil"></i>
                                    Edit
                                </a>
                                <form action="{{route('news.destroy',$news->id)}}" method="post"
                                      onsubmit="return confirm('are you sure?')">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE"/>
                                    <button class="btn btn-danger"><i class="fa fa-trash"></i>Delete </button>
                                </form>

                                <a href="{{route('frontend.postdetail',$news->slug)}}" class="btn btn-info">View Frontend</a>


                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->

            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>

    <!-- /.content -->
@endsection
@section('js')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready( function () {
            $('#datatable').DataTable();
        } );
    </script>
@endsection
