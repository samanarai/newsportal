<div class="form-group">
    {!!  Form::label('name', 'Name'); !!}
    {!! Form::text('name', null,['class' => 'form-control','id' => 'name','placeholder' => 'enter name']); !!}
    @include('includes.single_field_validation',['field'=>'name'])
</div>
<div class="form-group">
    {!!  Form::label('delegation', 'Delegation'); !!}
    {!! Form::text('delegation', null,['class' => 'form-control','id' => 'delegation','placeholder' => 'enter delegation']); !!}

    @include('includes.single_field_validation',['field'=>'delegation'])
</div>

<div class="form-group">
    {!!  Form::label('photo', ' Image'); !!}
        {!! Form::file('photo',null,['class' => 'form-control','id' => 'photo']); !!}
    @include('includes.single_field_validation',['field'=>'photo'])
    @if(isset($data['authors']) && $data['authors']->image)
        <img src="{{asset('images/author/' . $data['authors']->image)}}" width="70" height="70">
                {!! Form::hidden('photo', $data['authors']->image); !!}
    @endif
</div>

<div class="form-group">
    {!!  Form::label('status', 'Status'); !!}
    {!! Form::radio('status', '1') !!} Active
    {!! Form::radio('status', '0',true) !!} De Active
</div>