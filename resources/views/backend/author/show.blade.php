@extends('layouts.backend')
@section('title','Author Show page')
@section('content')
    <section class="content-header">
        <h1>
            Author Management
            <a href="{{route('author.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>

            <a href="{{route('author.index')}}" class="btn btn-info">
                <i class="fa fa-list"></i>
                List
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('author.index')}}">Author</a></li>
            <li class="active">Show page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Show Page</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('includes.flash')
               <table class="table table-bordered">
                   <thead>
                   <tr>
                       <th>Name</th>
                       <td>{{$data['authors']->name}}</td>

                   </tr>

                   <tr>
                       <th>Delegation</th>
                       <td>{{$data['authors']->delegation}}</td>
                   </tr>
                   <tr>
                       <th>Image</th>
                       <td>
                           <img src="{{asset("images/author/".$data['authors']->image)}}" alt=""></td>
                       </td>
                   </tr>

                   <tr>
                       <th>Status</th>
                       <td>
                           @if($data['authors']->status==1)
                               <span class="label label-success">Active</span>
                           @else
                               <span class="label label-danger">Deactive</span>
                               @endif
                       </td>
                   </tr>

                   <tr>
                       <th>Created At</th>
                       <td>{{$data['authors']->created_at->format('j F,Y ')}}</td>
                   </tr>
                   <tr>
                       <th>Updated At</th>
                       <td>{{$data['authors']->updated_at->format('j F,Y ')}}</td>
                   </tr>
                   <tr>
                       @foreach($data['author'] as $author)
                           <td>
                               <a href="{{route('author.edit',$author->id)}}" class="btn btn-warning">
                                   <i class="fa fa-pencil"></i>
                                   Edit
                               </a>
                           </td>
                       <td>
                               <form action="{{route('author.destroy',$author->id)}}" method="post"
                                     onsubmit="return confirm('Are you sure?')">
                                   @csrf
                                   <input type="hidden" name="_method" value="DELETE"/>
                                   <button class="btn btn-danger"><i class="fa fa-trash"></i>Delete</button>
                               </form>
                           </td>
                       @endforeach
                   </tr>
                   </thead>
               </table>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection