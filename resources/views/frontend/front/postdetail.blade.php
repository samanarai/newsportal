@extends('layouts.frontend')
@section('content')
@section('js')
    <script>
        function showAllComments(){
            $(".more-comments").show();
            $(".more-comments-btn").remove();
        }
    </script>
    <script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5d3306faba941200129ff9b5&product=custom-share-buttons"></script>
@endsection
    <!-- Post Header Section Start -->
    <div class="post-header-section section mt-30 mb-30">
        <div class="container">
            <div class="row row-1">

                <!-- Page Banner Start -->
                <div class="col-12">
                    <div class="post-header" style="background-image: url({{asset('images/pagebanner/page-banner-politic.jpg')}})">

                        <!-- Title -->
                        <h3 class="title">{{$data['postdetail'][0]->title}}</h3>

                        <!-- Meta -->
                        <div class="meta fix">

                            <a href="#" class="meta-item category fashion">{{$data['news']['category']->name}}</a>
                            {{--{{dd($data['news']['author'])}}--}}
                            <a href="#" class="meta-item author"><img src="{{asset('images/author/' . $data['news']['author']->image)}}" alt="post author">{{$data['news']['author']->name}}</a>
                            <span class="meta-item date"><i class="fa fa-clock-o"></i>{{$data['news'][0]->created_at->format('j F,Y ')}}</span>
                            <a href="#" class="meta-item comments"><i class="fa fa-comments"></i>{{count($data['comments'])}}</a>
                            <span class="meta-item view"><i class="fa fa-eye"></i>{{$data['news'][0]->viewscount}}</span>
                        </div>

                    </div>
                </div><!-- Post Header Section End -->

            </div>
        </div>
    </div><!-- Page Banner Section End -->

    <!-- Post Section Start -->
    <div class="post-section section">
        <div class="container">

            <!-- Feature Post Row Start -->
            <div class="row">

                <div class="col-lg-8 col-12 mb-50">

                    <!-- Post Block Wrapper Start -->
                    <div class="post-block-wrapper mb-50">

                        <!-- Post Block Body Start -->
                        <div class="body">
                            <div class="row">

                                <div class="col-12">

                                    <!-- Single Post Start -->
                                    <div class="single-post">
                                        <div class="post-wrap">

                                            <!-- Content -->
                                            <div class="content">

                                                <!-- Description -->


                                                <p>{!! $data['news'][0]->description !!}</p>



                                            </div>

                                            <div class="tags-social float-left">

                                                <div class="tags float-left">
                                                    <i class="fa fa-tags"></i>
                                                    @foreach ($data['postdetail'][0]->tags as $tag)
                                                        {{$tag->name}}
                                                    @endforeach
                                                    {{--<a href="#"></a>--}}
                                                    {{--<a href="#">Woman,</a>--}}
                                                    {{--<a href="#">Cool</a>--}}

                                                </div>




                                                {{--<div class="post-social float-right">--}}
                                                    {{--<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>--}}
                                                    {{--<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>--}}
                                                    {{--<a href="#" class="dribbble"><i class="fa fa-dribbble"></i></a>--}}
                                                    {{--<a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>--}}
                                                {{--</div>--}}

                                            </div>

                                        </div>
                                    </div><!-- Single Post End -->

                                </div>

                            </div>
                        </div><!-- Post Block Body End -->

                    </div><!-- Post Block Wrapper End -->

                    @php
                        $i=1;
                        $commentCount = count($data['comments']);
                    @endphp
                    @foreach($data['comments'] as $comment)
                        @if($i==3)
                            <div class="more-comments" style="display:none">
                            @endif
                    <!-- Post Author Start -->
                        <div class="post-author fix mb-50">

                            <div class="image float-left">{{substr($comment->name,0,1)}}</div>

                            <div class="content fix">

                                <h5><a href="#">{{$comment->name}}</a></h5>
                                <p>{{$comment->message}}</p>
                                {{--<div class="social">--}}
                                {{--<a href="{{$data['configurations']->fb_link}}" class="facebook"><i class="fa fa-facebook"></i></a>--}}
                                {{--<a href="{{$data['configurations']->twitter_link}}" class="twitter"><i class="fa fa-twitter"></i></a>--}}
                                {{--<a href="{{$data['configurations']->insta_link}}" class="dribbble"><i class="fa fa-dribbble"></i></a>--}}
                                {{--<a href="{{$data['configurations']->google_link}}" class="google-plus"><i class="fa fa-google-plus"></i></a>--}}
                                {{--</div>--}}
                            </div>

                        </div><!-- Post Author End -->
                                @if($i>=3 && $i==$commentCount)
                            </div>
                            <p class="text-center"><a href="javascript:void(0)" class="btn-link more-comments-btn" role="button" onclick="showAllComments()">+ View all Comments</a></p>
                        @endif
                        @php($i++)
                    @endforeach

                    <div class="post-block-wrapper">

                        <!-- Post Block Head Start -->
                        <div class="head">

                            <!-- Title -->
                            <h4 class="title">Leave a Comment</h4>

                        </div><!-- Post Block Head End -->

                        <!-- Post Block Body Start -->
                        <div class="body">
                            @include('includes.flash')
                            <div class="post-comment-form">
                                <form action="{{route('frontend.comment')}}" class="row" method="post" >
                                    @csrf
                                    {{--{{ dd($data['news'])}}--}}
                                    <input type="hidden" name="news_id" value="{{$data['news'][0]->id}}" >

                                    <div class="col-md-6 col-12 mb-20">
                                        <label for="name">Name <sup>*</sup></label>
                                        <input type="text" id="name" name="name" required="">
                                    </div>

                                    <div class="col-md-6 col-12 mb-20">
                                        <label for="email">Email <sup>*</sup></label>
                                        <input type="email" id="email" name="email" required="">
                                    </div>

                                    <div class="col-12 mb-20">
                                        <label for="website">Website <sup>*</sup></label>
                                        <input type="text" id="website" name="website" required="">
                                    </div>

                                    <div class="col-12 mb-20">
                                        <label for="message">Message <sup>*</sup></label>
                                        <textarea id="message" name="message"></textarea>
                                    </div>

                                    <div class="col-12">
                                        <input type="submit" value="Send Comment">
                                    </div>
                                </form>
                            </div>
                        </div><!-- Post Block Body End -->

                    </div><!-- Post Block Wrapper End -->

                </div>

                <!-- Sidebar Start -->
                <div class="col-lg-4 col-12 mb-50">
                    <div class="row">

                        <!-- Single Sidebar -->
                        <div class="single-sidebar col-lg-12 col-md-6 col-12">

                            <!-- Sidebar Block Wrapper -->
                            <div class="sidebar-block-wrapper">

                                <!-- Sidebar Block Head Start -->
                                <div class="head feature-head">

                                    <!-- Title -->
                                    <h4 class="title"></h4>

                                </div><!-- Sidebar Block Head End -->

                                <!-- Sidebar Block Body Start -->
                                <div class="body">
                                    @isset($data['advertisements'][3])
                                        <a href="#" class="sidebar-banner"><img src="{{asset('images/advertisement/' . $data['advertisements'][3]->image)}}" alt="Sidebar Banner"></a>
                                    @endisset
                                </div><!-- Sidebar Block Body End -->

                            </div>

                        </div>

                        <!-- Single Sidebar -->
                        <div class="single-sidebar col-lg-12 col-md-6 col-12">

                            <!-- Sidebar Banner -->
                            @isset($data['advertisements'][1])
                                <a href="#" class="sidebar-banner"><img src="{{asset('images/advertisement/' . $data['advertisements'][1]->image)}}" alt="Sidebar Banner"></a>
                            @endisset
                        </div>

                        <!-- Single Sidebar -->
                        <div class="single-sidebar col-lg-12 col-md-6 col-12">

                            <!-- Sidebar Banner -->
                            @isset($data['advertisements'][2])
                                <a href="#" class="sidebar-banner"><img src="{{asset('images/advertisement/' . $data['advertisements'][2]->image)}}" alt="Sidebar Banner"></a>
                            @endisset
                        </div>

                        <!-- Single Sidebar -->
                        <div class="single-sidebar col-lg-12 col-md-6 col-12">

                            <div class="sidebar-subscribe">
                                <h4>Subscribe To <br>Our <span>Update</span> News</h4>
                                <p>Adipiscing elit. Fusce sed mauris arcu. Praesent ut augue imperdiet, semper lorem id.</p>
                                <!-- Newsletter Form -->
                                <form action="http://devitems.us11.list-manage.com/subscribe/post?u=6bbb9b6f5827bd842d9640c82&amp;id=05d85f18ef" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="subscribe-form validate" target="_blank" novalidate>
                                    <div id="mc_embed_signup_scroll">
                                        <label for="mce-EMAIL" class="d-none">Subscribe to our mailing list</label>
                                        <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Your email address" required>
                                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6bbb9b6f5827bd842d9640c82_05d85f18ef" tabindex="-1" value=""></div>
                                        <button type="submit" name="subscribe" id="mc-embedded-subscribe" class="button">submit</button>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>
                </div><!-- Sidebar End -->

            </div><!-- Feature Post Row End -->

        </div>
    </div><!-- Post Section End -->
@endsection
